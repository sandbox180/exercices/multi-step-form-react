import {IAsset} from "./IAsset";

export interface IPortfolio {
  balanceUsd : number
  wallets : Array<IAsset>
}
