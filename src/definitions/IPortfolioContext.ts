import {Dispatch} from "./IDispatch";
import {IPortfolio} from "./IPortfolio";


export interface IPortfolioContext  {
  step: number
  portfolio : IPortfolio
  dispatchStep : Dispatch
  dispatchPortfolio : Dispatch
}
