import {IAction} from "./IAction";

export type Dispatch = (action: IAction) => void
