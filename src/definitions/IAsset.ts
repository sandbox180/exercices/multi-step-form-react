export interface IAsset {
  _id : string
  shortName : string
  name : string
  isLocked : boolean
  miniature: string
  allocation: number
  valueUsd : number
}
