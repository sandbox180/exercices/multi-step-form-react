export interface IAction<Data = unknown> {
  type : string,
  payload?: Data
}
