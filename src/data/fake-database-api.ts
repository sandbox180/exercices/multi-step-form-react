import {IAsset} from "../definitions/IAsset";
import * as jsonWallet from "./wallet.json";
import * as jsonBalance from "./balanceUsd.json";
import {IPortfolio} from "../definitions/IPortfolio";


export const fakeAPICall = {
  get : async (path : '/portfolio') : Promise<IPortfolio> =>  {

    const wallet : IAsset[] = JSON.parse(JSON.stringify(jsonWallet))['default']
    const balance : { balanceUsd: number } = JSON.parse(JSON.stringify(jsonBalance))
    return ({
      balanceUsd : +balance.balanceUsd,
      wallets : [...wallet]
    })
  }
}
