import {STEPS_ACTIONS} from "../constants/steping-const";
import {IAction} from "../definitions/IAction";

export const goToNextStep = () : IAction => ({
  type : STEPS_ACTIONS.next_step
})

export const goToPreviousStep = () : IAction => ({
  type : STEPS_ACTIONS.previous_step
})

export const jumpToStep = (step : number) : IAction<number> => ({
  type : STEPS_ACTIONS.jump_to_step,
  payload: step
})
