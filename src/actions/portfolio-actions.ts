import {WALLET_ACTION} from "../constants/portfolio-const";
import {IAsset} from "../definitions/IAsset";
import {IPortfolio} from "../definitions/IPortfolio";

export const editAssetFromWallet = (_id: string, fieldsToUpdate:Partial<IAsset> ) => ({
  type : WALLET_ACTION.edit_asset,
  payload: {_id,fieldsToUpdate}
})

export const deleteAssetFromWallet = (_id: string) => ({
  type : WALLET_ACTION.delete_asset_from_wallet,
  payload: _id
})

export const initPortfolio = (portfolio: IPortfolio) => ({
  type : WALLET_ACTION.init_portfolio,
  payload: portfolio
})
