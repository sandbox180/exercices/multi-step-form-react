import {IAction} from "../../definitions/IAction";
import {WALLET_ACTION} from "../../constants/portfolio-const";
import {IAsset} from "../../definitions/IAsset";
import {IPortfolio} from "../../definitions/IPortfolio";

const portfolioReducer = (state : IPortfolio, action : IAction) : IPortfolio  => {
  switch (action.type) {
    case WALLET_ACTION.init_portfolio : {
      return action.payload as IPortfolio
    }
    case WALLET_ACTION.edit_asset : {
      const {_id, fieldsToUpdate} = action.payload as {_id: string,fieldsToUpdate:Partial<IAsset> }
      const wallets = state.wallets.map(asset => {
        asset = asset._id === _id ? {...asset, ...fieldsToUpdate} : asset
        return asset
      })
      return {...state,wallets }
    }
    case WALLET_ACTION.delete_asset_from_wallet :{
      return {...state, wallets : state.wallets.filter(asset => asset._id !== action.payload)}
    }

    default:
      throw new Error(`Unhandled action type: ${action.type}`)
  }
}

export {portfolioReducer}
