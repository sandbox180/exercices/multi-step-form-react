import {IAction} from "../../definitions/IAction";
import {STEPS_ACTIONS} from "../../constants/steping-const";

const stepReducer = (state : number, action : IAction) : number => {
  switch (action.type) {
    case STEPS_ACTIONS.next_step :
      return  state + 1
    case STEPS_ACTIONS.previous_step:
      return state - 1
    case STEPS_ACTIONS.jump_to_step:
      return action.payload as number
    default:
      throw new Error(`Unhandled action type: ${action.type}`)
  }
}

export {stepReducer}
