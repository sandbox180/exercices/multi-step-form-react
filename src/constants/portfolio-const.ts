
export enum WALLET_ACTION {
  init_portfolio = "init_portfolio",
  add_asset_to_wallet = 'add_asset_to_wallet',
  edit_asset = 'edit_asset',
  delete_asset_from_wallet = 'delete_asset_from_wallet',
}
