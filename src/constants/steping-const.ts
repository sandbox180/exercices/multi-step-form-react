export const STEP_ENUM = {
  settings : 1,
  selection : 2,
  allocation : 3,
  mint : 4,
}

export enum STEPS_ACTIONS {
  next_step = 'next_step',
  previous_step = 'previous_step',
  jump_to_step = 'jump_to_step',
}
