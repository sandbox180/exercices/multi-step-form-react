import React from 'react';

import {usePortfolio} from "../../Context/PortfolioContext";
import {STEP_ENUM} from "../../../../constants/steping-const";
import {jumpToStep} from "../../../../actions/form-actions";

const ProgressBar = () => {

  const stepNames : Map<number,string> = new Map(Object.entries(STEP_ENUM).map(entry => [entry[1], entry[0]]))
  const stepList : number[] = Object.values(STEP_ENUM).sort((a,b) => a - b)
  const {step,dispatchStep} = usePortfolio()

  const handleJump = (index:number)=> {
    if(index < step)
      dispatchStep(jumpToStep(index))
  }

  const displayStep =  stepList.map(index => (
    <div key={index} className={`flex items-center ${index !== stepList.length ? 'w-full' : ''}
   pb-6`}>
      <div onClick={()=>handleJump(index)} className="cursor-pointer relative flex flex-col items-center text">

        <div className={`border-4 font-medium text-lg rounded-full transition 
        duration-500 ease-in-out h-12 w-12 flex items-center justify-center py-3 \
        ${step > index ? 'border-white bg-cyan-450 text-white' : step === index ? 
          'border-cyan-450 text-cyan-450' : "border-white bg-gray-175 text-white"}`}>
          {index}
        </div>
        <div className={`font-normal absolute top-0 text-center mt-12 w-32 font-semibold capitalize 
        ${step > index ? 'text-gray-400' : step === index ? 'text-cyan-450' : 'text-gray-175'}`}>
          {stepNames.get(index)}
        </div>
      </div>
      <div className={`flex-auto border-t-4 transition duration-500 ease-in-out ${step > index ? 'border-cyan-450' : ""}`}>{/*  Dipslay line */}</div>
    </div>
  ))

  return (
    <div className="w-5/6 mx-auto flex justify-between items-center">
      {displayStep}
    </div>
  )
}

export default ProgressBar;
