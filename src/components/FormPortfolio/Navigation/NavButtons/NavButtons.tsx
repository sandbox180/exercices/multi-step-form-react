import React from 'react';
import {usePortfolio} from "../../Context/PortfolioContext";
import {ChevronLeftIcon} from "@heroicons/react/outline";
import {goToNextStep, goToPreviousStep} from "../../../../actions/form-actions";
import {STEP_ENUM} from "../../../../constants/steping-const";
import {toast} from "react-toastify";

const NavButtons = () => {

  const stepCount : number = Object.values(STEP_ENUM).length
  const stepNames : Map<number,string> = new Map(Object.entries(STEP_ENUM).map(entry => [entry[1], entry[0]]))

  const {step,dispatchStep} = usePortfolio()
  const goNext = () => dispatchStep(goToNextStep())
  const goBack = () => dispatchStep(goToPreviousStep())
  const onSubmit = () => toast.success("Portfolia created")

  return (
    <div className="container flex justify-between items-center pt-5">
      {step !== 1 &&
        <button onClick={goBack} className="text-lg flex items-center transition duration-200 ease-in-out hover:scale-105">
          <ChevronLeftIcon className="block h-5 mr-2" />
          <div> Back to {stepNames.get(step - 1)} </div>
        </button>}
      {step < stepCount &&
        <button
        className="next-button"
        onClick={goNext} >Continue
      </button>}
      {step === stepCount &&
        <button className="next-button" onClick={onSubmit}>Submit</button>}
    </div>
  )
}

export default NavButtons;

/*
- Les steps sont scalable à l'infini, le code s'adapte sans avoir besoins de l'éditer.
- L'ordre des steps peut-être changé directement dans les constantes ... Le code lui s'adapte seul ! 😁
* */
