import Settings from "./Settings/Settings";
import Selection from "./Selection/Selection";
import Allocation from "./Allocation/Allocation";
import Mint from "./Mint/Mint";
import {usePortfolio} from "../Context/PortfolioContext";
import {STEP_ENUM} from "../../../constants/steping-const";


export const Steps = () => {


  const {step} = usePortfolio()
  const componentDictionary = new Map([
    [STEP_ENUM.settings, <Settings/>],
    [STEP_ENUM.selection, <Selection/>],
    [STEP_ENUM.mint, <Mint/>],
    [STEP_ENUM.allocation, <Allocation/>],

  ])


  return <>
    {componentDictionary.get(step)}
  </>
}
