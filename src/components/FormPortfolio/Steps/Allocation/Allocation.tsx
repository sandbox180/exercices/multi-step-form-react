import React from 'react';
import Card from "./Card/Card";
import {usePortfolio} from "../../Context/PortfolioContext";

const Allocation = () => {

  const {portfolio} = usePortfolio()

  return (
    <div>
     <section >
       <h3 className="form-title md:mb-2">Set assets allocations</h3>
       <p className="font-lightmd:leading-4 text-sm text-gray-400">Our smart contract will never rebalance your portfolio. <br/>
         Don't worry, you will be able to update your portfolio at any time once created.
       </p>
     </section>
      <section className="overflow-y-auto max-h-[36rem] md:max-h-[30rem] mt-2">
        {portfolio.wallets.map(asset => <Card
          key={asset._id}
          asset={asset}
        />)}
      </section>
    </div>


  );
};

export default Allocation;
