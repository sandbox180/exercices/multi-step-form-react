import React, {FunctionComponent, useEffect} from 'react';
import style from './RangeSlider.module.css'

interface IRangeSlider {
  disabled : boolean
  name : string
  onChange : (e : React.ChangeEvent<HTMLInputElement>) => void,
  value : number
  min : number
  max: number
  className : string
}

const RangeSlider : FunctionComponent<IRangeSlider> = ({className, disabled, name, onChange, value,min,max}) => {

  const rangeRef = React.createRef<HTMLInputElement>()

  useEffect(()=>{
    if(rangeRef.current)
      handleStyle(rangeRef.current)
  },[rangeRef])


  const handleStyle = (target : HTMLInputElement) => {
    const min : number = +target.min
    const max : number = +target.max
    const val : number = +target.value
    target.style.backgroundSize = (val - min) * 100 / (max - min) + '% 100%'
    if(!target.style.display)
      target.style.display = "block"
  }

  const handleInputChange = (e:React.ChangeEvent<HTMLInputElement>) => {
    onChange(e)
    if(!e.isDefaultPrevented())
      handleStyle(e.target)
  }

  return (
    <>
      <input
        ref={rangeRef}
        disabled={disabled}
        name={name}
        onChange={handleInputChange}
        type="range"
        value={value}
        min={min}
        max={max}
        className={`${disabled ? style.inputLockedRange : style.inputRange} ${className}`}

      />
    </>
  );
};

export default RangeSlider;
