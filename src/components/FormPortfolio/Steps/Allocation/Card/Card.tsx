import React, {FunctionComponent, useState} from 'react';
import {IAsset} from "../../../../../definitions/IAsset";
import {deleteAssetFromWallet, editAssetFromWallet} from "../../../../../actions/portfolio-actions";
import {usePortfolio} from "../../../Context/PortfolioContext";
import {Modale} from "../Modal/Modale";
import {LockClosedIcon, LockOpenIcon} from "@heroicons/react/outline";
import {TrashIcon} from "@heroicons/react/outline";
import RangeSlider from "../RangeSlider/RangeSlider";
import {toast} from "react-toastify";

interface ICardComponent {
  asset: IAsset,
}

const Card : FunctionComponent<ICardComponent> = ({asset}) => {

  const {portfolio,dispatchPortfolio} = usePortfolio()
  const [modalIsOpen, setModalIsOpen] = useState(false);

  const balanceUsd = portfolio.balanceUsd
  const allocationUsd = asset.allocation * asset.valueUsd
  const percentage = allocationUsd / balanceUsd * 100

  const handleWalletAllocation  = (e : React.ChangeEvent<HTMLInputElement>) => {
    const name  =  e.target.name as (keyof IAsset)
    const type =  e.target.type
    const value = type === 'checkbox' ? e.target.checked : +e.target.value
    if(name ===  "allocation"){
      const newAllocationUsd = portfolio.wallets.reduce((prev, curr)=> {
        return curr._id === asset._id ?   prev + (curr.valueUsd * +value )  : prev + (curr.valueUsd * curr.allocation)
      },0)

      if(newAllocationUsd > balanceUsd){

        e.preventDefault()
        return
      }
    }

    dispatchPortfolio(editAssetFromWallet(asset._id,{[name]: value}))
  }

  const handleDelete  = () => {
    dispatchPortfolio(deleteAssetFromWallet(asset._id))
    setModalIsOpen(false)
    toast.success("Asset removed")
  }

  return (
    <div className="flex-row
    h-68 md:h-24 flex justify-between items-center md:items-stretch bg-gray-75 my-4 rounded-md">
      {/*Main Section*/}
      <div className="px-4 flex-1 flex flex-col justify-center">
        {/*Infos*/}
        <section className="flex flex-col md:flex-row justify-between items-center mb-3">

          {/*section 1*/}
          <div className="flex my-3 md:my-0  flex-1">
            <img className="block max-h-12 w-auto h-full rounded-full" alt="" src={asset.miniature} />
            <div className="flex ml-2 flex-col justify-center">
                <p  className="bold">{asset.shortName}</p>
                <p className="light">{asset.name}</p>
            </div>
          </div>

          {/*Section 2*/}
          <div className="
          sm:flex-row sm:items-start md:items-center
          flex my-3 md:my-0 flex-1 flex-col md:flex-col items-center">
            <div className="bold sm:mr-2 md:mr-0">{percentage.toFixed(2)} %</div>
            <label className="light cursor-pointer">
              <input
                className="hidden"
                checked={asset.isLocked}
                type="checkbox"
                name="isLocked"
                onChange={handleWalletAllocation}
              />
              <div className={`flex items-center transition duration-200 ease-in-out hover:scale-105 
              ${asset.isLocked ? "text-amber-500": ""}`}>
                <p>{asset.isLocked ? "Locked" : "Unlocked"}</p>
                <i>
                   {asset.isLocked ?
                     <LockClosedIcon
                       className="scale-150 block h-3 ml-2"
                       aria-hidden="true" /> :
                     <LockOpenIcon
                       className="scale-150 block h-3 ml-2"
                       aria-hidden="true" />
                   }
                </i>

              </div>
            </label>
          </div>

          {/*Section 3*/}
          <div className="
          sm:flex-row md:flex-col sm:items-start md:items-center
          flex my-3 md:my-0  flex-1 flex-col items-center">
            <div className="leading-3 mb-2  sm:mr-2 md:mr-0">{asset.allocation} {asset.shortName}</div>
            <div className="light">≈ ${allocationUsd.toFixed(2)} </div>
          </div>

        </section>
        {/*Range barre*/}
        <section>
          <RangeSlider
            disabled={asset.isLocked}
            name="allocation"
            onChange={handleWalletAllocation}
            value={asset.allocation}
            min={0}
            max={balanceUsd / asset.valueUsd}
            className="w-full h-4 mb-4 md:mb-0"
          />
        </section>
      </div>

      {/*DeleteButton*/}
      <div className="flex ">
      <Modale
        open={modalIsOpen}
        handleDelete={handleDelete}
        setOpen={setModalIsOpen}
      />
      <button
        className="border-l px-5 md:px-0 py-2 my-2"
        onClick={()=> setModalIsOpen(true) }>
        <TrashIcon
          className="m-2 h-8 w-auto text-gray-400
          transition duration-200 ease-in-out hover:scale-105 hover:text-red-500"
          aria-hidden="true"
        />
      </button>
      </div>

    </div>
  );
};

export default Card;
