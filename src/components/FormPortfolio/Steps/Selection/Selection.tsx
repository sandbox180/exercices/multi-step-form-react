import React from 'react';
import {toast} from "react-toastify";

const Selection = () => {

  const generateUnexpectedError = () => {
    toast.error("Opps... Error (or Not)")
  }

  const generateUserError = () => {
    toast.warning("Invalid information provided")
  }

  return (
    <div>
      <h3 className="form-title">Selection</h3>
      <button
        className="error-generator bg-orange-400"
        onClick={generateUserError}
        name={"user_erreur"}
      >
        Generate user error
      </button>
      <button
        className="error-generator bg-red-400"
        onClick={generateUnexpectedError}
      >
        Generate unexpected error</button>
    </div>
  );
};

export default Selection;
