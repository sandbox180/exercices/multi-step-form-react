import React, {createContext, FunctionComponent, useEffect, useReducer} from 'react';
import {stepReducer} from "../../../reducers/step-reducer/step-reducer";
import {IPortfolioContext} from "../../../definitions/IPortfolioContext";
import {portfolioReducer} from "../../../reducers/portfolioReducer/portfolio-reducer";
import {fakeAPICall} from "../../../data/fake-database-api";
import {initPortfolio} from "../../../actions/portfolio-actions";


const PortfolioContext = createContext<IPortfolioContext | null >(null)

const PortfolioProvider : FunctionComponent = ({children}) => {

  useEffect(()=> {
    makeInit()
  },[])

  const makeInit= async () => {
    const result = await fakeAPICall.get("/portfolio")
    dispatchPortfolio(initPortfolio(result))
  }
  const [step, dispatchStep] = useReducer(stepReducer,  1)
  const [portfolio, dispatchPortfolio] = useReducer(portfolioReducer, {
    balanceUsd: NaN,
    wallets : []
  })

  const value : IPortfolioContext = {step, portfolio, dispatchStep, dispatchPortfolio}
  return (
      <PortfolioContext.Provider value={value} >
        {children}
      </PortfolioContext.Provider>
  )
}

const usePortfolio = () => {
  const context = React.useContext(PortfolioContext)
  if (!context)
    throw new Error('useStepContext must be used within a StepProvider')

  return context
}

export {PortfolioProvider, usePortfolio}
