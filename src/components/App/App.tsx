import React from 'react';
import './App.css';
import {Steps} from "../FormPortfolio/Steps/Steps";
import {PortfolioProvider} from "../FormPortfolio/Context/PortfolioContext";
import ProgressBar from "../FormPortfolio/Navigation/ProgressBar/ProgressBar";
import NavButtons from "../FormPortfolio/Navigation/NavButtons/NavButtons";
import ErrorBoundary from "../Error/ErrorBoundary";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
<ErrorBoundary>
  <ToastContainer limit={3} autoClose={2000} />
  <div  className="app flex content-center justify-center relative">
    <aside>
      <div style={{transform : "translate(-50%, -50%) scale(0.95)"}}
           className="hidden md:block  z-30 absolute top-1/2 left-1/2 bg-gray-50 h-5/6 w-[42rem] shadow-gray-300 shadow-xl">
      </div>
      <div style={{transform : "translate(-56%, -50%) scale(0.92)"}}
           className="hidden md:block  z-20 absolute top-1/2 left-1/2 bg-gray-50 h-5/6 w-[42rem] shadow-gray-300 shadow-xl ">
      </div>
    </aside>

    <section className="flex flex-col justify-between z-40 md:min-h-[39rem]
      w-full md:w-8/12 md:max-w-xl md:my-20 bg-white mx-auto shadow-xl rounded-sm py-4 px-6
      shadow-gray-300 shadow-xl
      ">
      <PortfolioProvider>
        <div className="md:mb-5 mb-7">
          <ProgressBar />
        </div>

        <div>
          <Steps/>
        </div>
        <div>
          <div style={{height:"0.1rem"}}
               className="mt-2 bg-gradient-to-r from-cyan-300 via-indigo-700 via-purple-700 via-pink-300 to-yellow-200">
          </div>
          <NavButtons />
        </div>



      </PortfolioProvider>
    </section>
  </div>
  </ErrorBoundary>
  );
}

export default App;
