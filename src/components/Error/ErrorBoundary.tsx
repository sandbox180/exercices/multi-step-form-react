import React, {Component, ErrorInfo, ReactNode} from "react";
import {toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

interface Props {
  children: ReactNode;
}

interface State {
  hasError: boolean;
}

class ErrorBoundary extends Component<Props, State> {
  public state: State = {
    hasError: false
  };

  public static getDerivedStateFromError(error: Error): State {
    return { hasError: true };
  }

  public componentDidCatch(error: Error, errorInfo: ErrorInfo) : void {
      toast.error('Oops.. an unexpected error occurred');
  }

  public render() {
    return <>
      {this.props.children}
    </>
  }
}

export default ErrorBoundary;
