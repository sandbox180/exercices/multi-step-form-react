class UserError extends Error{
  type = "user_error"
}

export {UserError}
