# Infos :

##### Data 
- balance ( en $ ) dans `src/data/balanceUsd.json`
- wallets dans `src/data/wallet.json`

##### Config 
- StepBarre Add/Remove step : `src/constants/steping-const` 
- Carousel : Add/Remove step : `src/components/FormPortfolio/Steps/Steps.tsx`

# Features :
##### User Features :
- :heavy_check_mark: Responsive Design 
- :heavy_check_mark: Jump à travers les étapes depuis barre d'étapes.
- :heavy_check_mark: Bouton étape suivante / étape précédente.
- :heavy_check_mark: Bouton envoyer.
- :heavy_check_mark: Gestion des erreurs

##### Allocation step : 
- :heavy_check_mark: Vérification allocation total (allocation ne peux pas dépasser 100% de la balance )
- :heavy_check_mark: Suppression asset du portfolio 
- :heavy_check_mark: Verrouillage asset du portfolio

##### Coding Features : (React + Typescript + Tailwindcss)
- :heavy_check_mark: Structure par composants ( Angular like )
- :heavy_check_mark: Utilisation des Contexts (Facilement interchangeable par Redux)
- :heavy_check_mark: Barre d'étape autoscaling = Ajout/Retrait d'étapes sans toucher au code --> Uniquement Via fichier de config
- :heavy_check_mark: Utilisations de reducers = Modularité + Testabilité
- :heavy_check_mark: Fake call api pour récupération de la data

##### Not implemented :
- :x: Animation carousel entre chaque steps.
