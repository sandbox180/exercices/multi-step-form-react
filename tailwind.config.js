module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend : {
      fontFamily: {
        sans : "SofiaPro"
      },
      colors: {
        'cyan-450': "#2BD4DB",
        'gray-75': "#F9FAFC",
        'gray-175': "#EAEDF3"
      },
      fontSize: {
        '4.5xl' : "2.5rem"
      }
    }
  },
  plugins: [],
}
